package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.AccountLoginPage2;

public class TC001_LoginPage extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
	testCaseName ="TC001_LoginPage";
	testDescription ="Login into acme";
	testNodes="ui path";
	author ="Krithi";
	category="Smoke";
	dataSheetName="TC001";
	}

	@Test
	public void login() {
	new AccountLoginPage2()
	.enterEmail()
	.enterpassword()
	.clickLogin()
	.MouseoverVendors()
	.clickSearchVendor()
	.EnterVendorTaxid()
	.Clicksearch()
	.GetvdrName();
	

}
}