package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchResultPage extends ProjectMethods {
	
public VendorSearchResultPage(){	
PageFactory.initElements(driver, this);
}
@FindBy(how=How.XPATH, using="//table[@class='table']/tbody/tr[2]/td[1]") WebElement vdtext;

public VendorSearchResultPage GetvdrName() {
getElementText(vdtext);
verifyExactText(vdtext, "Maxitronic Limited");
return this;
}	
}	
	


