package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class AccountLoginPage2 extends ProjectMethods{
	
public AccountLoginPage2() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
}	

@FindBy(how= How.XPATH, using="//input[@id='email']") WebElement mail;
@FindBy(how=How.XPATH, using="//input[@id='password']") WebElement pwd;
@FindBy(how=How.XPATH, using="//button[@id='buttonLogin']") WebElement log;

public AccountLoginPage2 enterEmail(){
clearAndType(mail, "mailtokerthi@gmail.com");	 

return this;
}



public AccountLoginPage2 enterEmail1(){
clearAndType(mail, "mailtokerthi@gmail.com");	 

return this;
}
public AccountLoginPage2 enterpassword(){
clearAndType(pwd, "Remember@07");	
return this;	
}
public DashboardPage clickLogin() {
click(log);
return new DashboardPage();


}
}


