package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DashboardPage extends ProjectMethods{
	
public DashboardPage() {

PageFactory.initElements(driver, this);
}

@FindBy(how=How.XPATH, using="(//button[@class='btn btn-default btn-lg'])[4]") WebElement vendor;
@FindBy(how=How.LINK_TEXT, using="Search for Vendor") WebElement src;

public DashboardPage MouseoverVendors() {
Actions ac = new Actions(driver);
ac.moveToElement(vendor).build().perform();
return this;	
	
}

public VendorsPage clickSearchVendor() {
click(src);	
return new VendorsPage();
	
}



















}


