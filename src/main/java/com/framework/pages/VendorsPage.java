package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorsPage extends ProjectMethods {
	
public VendorsPage() {
	
PageFactory.initElements(driver, this);
		
}	
@FindBy(how=How.XPATH, using="//input[@id='vendorTaxID']") WebElement Vdrtax;
@FindBy(how=How.ID, using="buttonSearch") WebElement search;

public VendorsPage EnterVendorTaxid() {
clearAndType(Vdrtax, "DE767565");	
return this;
}

public VendorSearchResultPage Clicksearch() {
click(search);
return new VendorSearchResultPage();
	
}

}

