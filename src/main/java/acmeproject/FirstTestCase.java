package acmeproject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FirstTestCase {
	
public static void main(String args[]) {
	
//Open browser
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
ChromeDriver driver = new ChromeDriver();

driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

//Load url
driver.get("https://acme-test.uipath.com/account/login");

//login steps
driver.findElementByXPath("//input[@id='email']").sendKeys("mailtokerthi@gmail.com");
driver.findElementByXPath("//input[@id='password']").sendKeys("Remember@07");
driver.findElementByXPath("//button[@id='buttonLogin']").click();

WebElement vndract = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
Actions ac = new Actions(driver);
ac.moveToElement(vndract).build().perform();
driver.findElementByLinkText("Search for Vendor").click();

driver.findElementByXPath("//input[@id='vendorTaxID']").sendKeys("DE767565");
driver.findElementById("buttonSearch").click();
WebElement vdrname = driver.findElementByXPath("//table[@class='table']/tbody/tr[2]/td[1]");
String name= vdrname.getText();
System.out.println(name);

driver.close();


}

}
